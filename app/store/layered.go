package store

import (
	"context"
)

// Layered sturct
type Layered struct {
	ctx           context.Context
	storeSupplier *Supplier
}

// NewLayered function create the new instance of layered store.
func NewLayered(options ...Option) Store {
	layered := &Layered{
		ctx:           context.TODO(),
		storeSupplier: NewSupplier(),
	}

	for _, option := range options {
		option(layered)
	}

	return layered
}

// Contacts function returns the IMAPIContact.
func (s *Layered) Contacts() IMAPIContact {
	return s.storeSupplier.Contact()
}
