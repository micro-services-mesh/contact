package store

import "gitlab.com/snehaldangroshiya/contacts/model"

// Result struct
type Result struct {
	Data interface{}
	Err  *model.AppError
}

// Channel type
type Channel chan Result

// Store interface
type Store interface {
	Contacts() IMAPIContact
}

type IMAPIContact interface {
	Create(collection string, contactProps *model.Contact) Channel
	GetContact(collection string, id string) Channel
	CreateFolder() Channel
	Update() Channel
	Delete(collection string, id string) Channel
	List(collection string) Channel
	ListFolder() Channel
}

type IMAPIExtension interface {
	Create()
	Get()
	Update()
	Delete()
}
