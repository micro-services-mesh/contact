package store

import (
	"log"
	"net/http"

	"gitlab.com/snehaldangroshiya/contacts/model"
	"go.mongodb.org/mongo-driver/bson"
)

// Contact struct
type Contact struct {
	transport
}

func (c *Contact) Create(collection string, contact *model.Contact) Channel {
	storeChannel := make(Channel, 1)
	go func() {
		result := Result{}
		c := c.connect()

		contact.PreSave()
		p, err := contact.ToBson(contact)
		if err != nil {
			result.Err = model.NewAppError("DBItemStore.Create", "store.db_item.create.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		} else if err := c.CreateDocument(collection, p); err != nil {
			result.Err = model.NewAppError("DBItemStore.Create", "store.db_item.create.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		}
		result.Data = contact
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (c *Contact) GetContact(collection string, id string) Channel {
	storeChannel := make(Channel, 1)
	go func() {
		result := Result{}
		c := c.connect()

		contactModel := &model.Contact{}
		item, err := c.FindOne(collection, bson.D{{"id", id}})

		if err != nil {
			result.Err = model.NewAppError("DBItemStore.GetContact", "store.db_item.getcontact.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		} else {
			contact, err := contactModel.FromBson(item, contactModel)
			if err != nil {
				result.Err = model.NewAppError("DBItemStore.GetContact", "store.db_item.getcontact.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
			}
			log.Println(contact)
			result.Data = contact
		}

		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (c *Contact) CreateFolder() Channel {
	panic("implement me")
}

func (c *Contact) Update() Channel {
	panic("implement me")
}

func (c *Contact) Delete(collection string, id string) Channel {
	panic("implement me")
}

func (c *Contact) List(collection string) Channel {
	storeChannel := make(Channel, 1)
	go func() {
		result := Result{}
		c := c.connect()

		contacts, err := c.Search(collection, nil, 0, 0)
		if err != nil {
			result.Err = model.NewAppError("DBItemStore.Create", "store.db_item.create.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		}

		var items []*model.Contact
		for _, contact := range contacts {
			con := &model.Contact{}
			item, err := con.FromBson(contact, con)

			if err != nil {
				result.Err = model.NewAppError("DBItemStore.Create", "store.db_item.create.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
			}

			items = append(items, item.(*model.Contact))
		}

		result.Data = items
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (c *Contact) ListFolder() Channel {
	panic("implement me")
}

// NewContactStore func
func NewContactStore(transport transport) IMAPIContact {
	return &Contact{transport}
}
