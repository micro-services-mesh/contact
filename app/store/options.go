package store

// Option type
type Option func(l *Layered)

// WithBackendEndPoint func
func WithBackendEndPoint(backendEndpoint string) Option {
	return func(l *Layered) {
		l.storeSupplier.transport.BackendEndpoint = backendEndpoint
	}
}

// WithDatabaseName func
func WithDatabaseName(databaseName string) Option {
	return func(l *Layered) {
		l.storeSupplier.transport.DatabaseName = databaseName
	}
}
