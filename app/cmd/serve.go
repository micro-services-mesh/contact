/*
Copyright © 2022 Snehal Dangroshiya <snehaldangroshiya@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"context"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/snehaldangroshiya/contacts/bootstrap"
	"gitlab.com/snehaldangroshiya/contacts/config"
)

const (
	defaultListenAddr = "127.0.0.1:8774"
	basePath          = "/api/v1"
	defaultDBURI      = "mongodb://0.0.0.0:27017/?retryWrites=false"
	defaultDatabase   = "contact"
)

var bootstrapConfig = &bootstrap.Config{}

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Contact service started",
	Run: func(cmd *cobra.Command, args []string) {
		if err := serve(cmd, args); err != nil {
			fmt.Printf("Error: %v \n\n", err)
			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(serveCmd)

	cfg := bootstrapConfig
	serveCmd.Flags().StringVar(&cfg.Listen, "listen", getEnv("CONTACT_LISTEN", defaultListenAddr), fmt.Sprintf("TCP listen address (default \"%s\").", "8774"))
	serveCmd.Flags().StringVar(&cfg.BasePath, "api_base", getEnv("CONTACT_BASE_API", basePath), "Base api path for the stock service.")
	serveCmd.Flags().StringVar(&cfg.BackendEndPoint, "backend_url", getEnv("CONTACT_BACKEND_ENDPOINT", defaultDBURI), "Backend end point of stock service.")
	serveCmd.Flags().StringVar(&cfg.DatabaseName, "database_name", getEnv("CONTACT_DATABASE", defaultDatabase), "Database name which used by the stock service.")
	serveCmd.Flags().Bool("log-timestamp", true, "Prefix each log line with timestamp")
	serveCmd.Flags().String("log-level", "info", "Log level (one of panic, fatal, error, warn, info or debug)")

}

func serve(cmd *cobra.Command, args []string) error {
	ctx := context.Background()
	logTimestamp, _ := cmd.Flags().GetBool("log-timestamp")
	logLevel, _ := cmd.Flags().GetString("log-level")

	logger, err := newLogger(!logTimestamp, logLevel)
	if err != nil {
		return fmt.Errorf("failed to create logger: %v", err)
	}
	logger.Infoln("serve start")

	return bootstrap.Boot(ctx, bootstrapConfig, &config.Config{
		Logger: logger,
	})
}
