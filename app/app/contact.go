package app

import (
	"gitlab.com/snehaldangroshiya/contacts/model"
	"gitlab.com/snehaldangroshiya/contacts/utils"
)

const tableContacts = "contacts"

// CreateContact func
func (app *App) CreateContact(contact *model.Contact) (*model.Contact, *model.AppError) {
	result := <-app.store.Contacts().Create(tableContacts, contact)
	if result.Err != nil {
		app.logger.Errorln(utils.T("store.message_store.create.ipm_subtree.app_error"))
		return nil, result.Err
	}
	return result.Data.(*model.Contact), nil
}

// GetContacts func
func (app *App) GetContacts() ([]*model.Contact, *model.AppError) {
	result := <-app.store.Contacts().List(tableContacts)
	if result.Err != nil {
		app.logger.Errorln(utils.T("store.message_store.create.ipm_subtree.app_error"))
		return nil, result.Err
	}

	return result.Data.([]*model.Contact), nil
}

// GetContact func
func (app *App) GetContact(id string) (*model.Contact, *model.AppError) {
	result := <-app.store.Contacts().GetContact(tableContacts, id)
	if result.Err != nil {
		app.logger.Errorln(utils.T("store.message_store.create.ipm_subtree.app_error"))
		return nil, result.Err
	}

	return result.Data.(*model.Contact), nil
}
