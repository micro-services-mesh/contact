package app

import (
	"context"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/snehaldangroshiya/contacts/server"
	"gitlab.com/snehaldangroshiya/contacts/store"
	"gitlab.com/snehaldangroshiya/contacts/utils"
)

// Impl inter
type Impl interface {
	InitStore(ctx context.Context)
	StartServer(ctx context.Context) error
}

// App struct
type App struct {
	srv             *server.Server
	store           store.Store
	basePath        string
	databaseName    string
	backendEndPoint string
	logger          logrus.FieldLogger
}

// NewApp func
func NewApp(optios ...Option) Impl {
	return newApp(optios...)
}

func newApp(options ...Option) *App {
	a := &App{}
	for _, option := range options {
		option(a)
	}

	if err := utils.TranslationsPreInit(); err != nil {
		a.logger.Errorln("Unable to initialize the localization.")
	}

	return a
}

// GetRouter return the router.
func (a *App) GetRouter() *mux.Router {
	return a.srv.Router
}

// InitStore initialize the store.
func (a *App) InitStore(ctx context.Context) {
	a.store = store.NewLayered(
		store.WithBackendEndPoint(a.backendEndPoint),
		store.WithDatabaseName(a.databaseName),
	)
	a.logger.WithField("databaseName", a.databaseName).Infoln("database configured")
}

// StartServer func
func (a *App) StartServer(ctx context.Context) error {
	return a.srv.StartServer(ctx)
}
