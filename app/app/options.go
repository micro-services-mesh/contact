package app

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/snehaldangroshiya/contacts/server"
)

// Option type
type Option func(a *App)

// WithBasePath func
func WithBasePath(basePath string) Option {
	return func(a *App) {
		a.basePath = basePath
	}
}

// WithDBName func
func WithDBName(databaseName string) Option {
	return func(a *App) {
		a.databaseName = databaseName
	}
}

// WithBackendEndPoint func
func WithBackendEndPoint(backendEndPoint string) Option {
	return func(a *App) {
		a.backendEndPoint = backendEndPoint
	}
}

// WithServer func
func WithServer(server *server.Server) Option {
	return func(a *App) {
		a.srv = server
	}
}

// WithLogger func
func WithLogger(logger logrus.FieldLogger) Option {
	return func(a *App) {
		a.logger = logger
	}
}
