package api

import (
	"net/http"

	"gitlab.com/snehaldangroshiya/contacts/model"

	"github.com/gorilla/mux"
)

// InitContacts func
func (api *API) InitContacts(contacts *mux.Router) {
	contacts.Handle("", api.APIHandler(createContact)).Methods("POST")
	contacts.Handle("", api.APIHandler(getContacts)).Methods("GET")
	contacts.Handle("/{id:[A-Za-z0-9_-]+}", api.APIHandler(getContact)).Methods("GET")
	contacts.Handle("/{id:[A-Za-z0-9_-]+}", api.APISessionRequired(contactDelete)).Methods("DELETE")
	contacts.Handle("/{id:[A-Za-z0-9_-]+}", api.APISessionRequired(contactUpdate)).Methods("PATCH")
}

func createContact(c *Context, w http.ResponseWriter, r *http.Request) {
	var contact = &model.Contact{}
	contactModel, modelErr := contact.FromJson(r.Body)
	if modelErr != nil {
		c.Err = model.NewAppError("api.createContact", "json.conversation.app_error", nil, "id="+modelErr.Error(), http.StatusBadRequest)
		return
	}

	props, createUserErr := c.App.CreateContact(contactModel.(*model.Contact))
	if createUserErr != nil {
		c.Err = createUserErr
		return
	}

	w.WriteHeader(http.StatusCreated)
	result, jsonErr := props.ToJson(props)
	if jsonErr != nil {
		c.Err = model.NewAppError("api.createContact", "json.conversation.app_error", nil, "id="+jsonErr.Error(), http.StatusBadRequest)
		return
	}
	w.Write(result)
}

func getContacts(c *Context, w http.ResponseWriter, r *http.Request) {
	contacts, createUserErr := c.App.GetContacts()
	if createUserErr != nil {
		c.Err = createUserErr
		return
	}

	contact := &model.Contact{}
	result, err := contact.ToJson(&contacts)
	if err != nil {
		c.Err = model.NewAppError("api.createContact", "json.conversation.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write(result)
}

func getContact(c *Context, w http.ResponseWriter, r *http.Request) {
	param := mux.Vars(r)
	contact, modelErr := c.App.GetContact(param["id"])
	if modelErr != nil {
		c.Err = modelErr
	}

	result, err := contact.ToJson(contact)
	if err != nil {
		c.Err = model.NewAppError("api.getContact", "json.getContact.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(result)
}

func contactDelete(c *Context, w http.ResponseWriter, r *http.Request) {

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("contact contactDelete"))
}

func contactUpdate(c *Context, w http.ResponseWriter, r *http.Request) {

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("contact contactUpdate"))
}
