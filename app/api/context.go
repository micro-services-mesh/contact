package api

import (
	"net/http"

	"gitlab.com/snehaldangroshiya/contacts/utils"

	"gitlab.com/snehaldangroshiya/contacts/app"
	"gitlab.com/snehaldangroshiya/contacts/model"
)

type handler struct {
	app            *app.App
	handleFunc     func(*Context, http.ResponseWriter, *http.Request)
	requireSession bool
	trustRequester bool
	requireMfa     bool
}

// Context struct
type Context struct {
	App       *app.App
	T         model.TranslateFunc
	Err       *model.AppError
	RequestID string
	IPAddress string
	Path      string
	//Session       *model.Session
	siteURLHeader string
}

// APIHandler func
func (api *API) APIHandler(h func(*Context, http.ResponseWriter, *http.Request)) http.Handler {
	return &handler{
		app:            api.App,
		handleFunc:     h,
		requireSession: false,
		trustRequester: false,
		requireMfa:     false,
	}
}

// APISessionRequired func
func (api *API) APISessionRequired(h func(*Context, http.ResponseWriter, *http.Request)) http.Handler {
	return &handler{
		app:            api.App,
		handleFunc:     h,
		requireSession: true,
		trustRequester: false,
		requireMfa:     true,
	}
}

// ServeHTTP func
func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c := &Context{
		App:       h.app,
		T:         utils.T,
		RequestID: model.NewID(),
		IPAddress: utils.GetIPAddress(r),
		Path:      r.URL.Path,
	}

	w.Header().Set(model.HeaderRequestID, c.RequestID)
	w.Header().Set("Content-Type", "application/json")

	// TODO: check Authorization headers.

	if c.Err == nil {
		h.handleFunc(c, w, r)
	}

	if c.Err != nil {
		c.Err.Translate(c.T)
		c.Err.RequestID = c.RequestID
		c.Err.Where = r.URL.Path

		w.WriteHeader(c.Err.StatusCode)
		w.Write([]byte(c.Err.ToJSON()))
	}
}
