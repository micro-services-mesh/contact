package api

import (
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/snehaldangroshiya/contacts/app"
)

// Options type
type Options func(a *API)

// WithAPP func
func WithAPP(app *app.App) Options {
	return func(a *API) {
		a.App = app
	}
}

// WithBasePath func
func WithBasePath(basePath string) Options {
	return func(a *API) {
		a.basePath = basePath
	}
}

// WithRouter func
func WithRouter(root *mux.Router) Options {
	return func(a *API) {
		a.rootRouter = root
	}
}

// WithLogger func
func WithLogger(logger logrus.FieldLogger) Options {
	return func(a *API) {
		a.logger = logger
	}
}
