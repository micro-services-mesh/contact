package api

import (
	"strings"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/snehaldangroshiya/contacts/app"
)

type router = map[string]*mux.Router

// API struct
type API struct {
	App        *app.App
	BaseRoutes router
	basePath   string
	rootRouter *mux.Router
	logger     logrus.FieldLogger
}

// Init func
func Init(options ...Options) *API {
	api := &API{
		BaseRoutes: make(router),
	}

	for _, option := range options {
		option(api)
	}

	api.logger.WithField("basePath", api.basePath).Infoln("Api configured with")

	api.BaseRoutes["ApiRoot"] = api.rootRouter.PathPrefix(api.basePath).Subrouter()
	baseRoutes := api.BaseRoutes["ApiRoot"]

	api.BaseRoutes["Contact"] = baseRoutes.PathPrefix("/contact").Subrouter()

	api.InitContacts(api.BaseRoutes["Contact"])

	var apiNames []string
	for s, _ := range api.BaseRoutes {
		if s != "ApiRoot" {
			apiNames = append(apiNames, s)
		}
	}

	logrus.WithField("api", strings.Join(apiNames, ",")).Infoln("Initialized api")
	return api
}
