package bootstrap

import (
	"context"

	"gitlab.com/snehaldangroshiya/contacts/api"
	"gitlab.com/snehaldangroshiya/contacts/app"
	"gitlab.com/snehaldangroshiya/contacts/config"
	"gitlab.com/snehaldangroshiya/contacts/server"

	"github.com/sirupsen/logrus"
)

// Config struct
type Config struct {
	BasePath        string
	Listen          string
	BackendEndPoint string
	DatabaseName    string
	Logger          logrus.FieldLogger
}

type bootstrap struct {
	cfg *config.Config
	app app.Impl
}

func (b *bootstrap) Config() *config.Config {
	return b.cfg
}

// Boot func
func Boot(ctx context.Context, cfg *Config, serverCfg *config.Config) error {
	bs := &bootstrap{
		cfg: serverCfg,
	}

	if err := bs.setup(ctx, cfg); err != nil {
		return err
	}

	return nil
}

func (b *bootstrap) setup(ctx context.Context, cfg *Config) error {
	app := app.NewApp(
		app.WithDBName(cfg.DatabaseName),
		app.WithBasePath(cfg.BasePath),
		app.WithBackendEndPoint(cfg.BackendEndPoint),
		app.WithLogger(b.cfg.Logger),
		app.WithServer(server.NewServer(b.cfg.Logger, cfg.Listen)),
	).(*app.App)

	api.Init(
		api.WithAPP(app),
		api.WithBasePath(cfg.BasePath),
		api.WithRouter(app.GetRouter()),
		api.WithLogger(b.cfg.Logger),
	)
	app.InitStore(ctx)
	app.StartServer(ctx)
	return nil
}
