package model

import (
	"encoding/base64"
	"io"
	"log"

	"github.com/pborman/uuid"
)

type EmailAddress struct {
	Address string `json:"address,omitempty" bson:"address,omitempty"`
	Name    string `json:"name,omitempty" bson:"name,omitempty"`
}

type PhysicalAddress struct {
	City            string `json:"city,omitempty" bson:"city,omitempty"`
	CountryOrRegion string `json:"country_or_region,omitempty" bson:"country_or_region,omitempty"`
	PostalCode      string `json:"postal_code,omitempty" bson:"postal_code,omitempty"`
	State           string `json:"state,omitempty" bson:"state,omitempty"`
	Street          string `json:"street,omitempty" bson:"street,omitempty"`
}

// Contact struct
type Contact struct {
	ID                   string           `json:"id,omitempty" bson:"id,omitempty"`
	AssistantName        string           `json:"assistantName,omitempty" bson:"assistant_name,omitempty"`
	Birthday             string           `json:"birthday,omitempty" bson:"birthday,omitempty"`
	BusinessAddress      *PhysicalAddress `json:"businessAddress,omitempty" bson:"business_address,omitempty"`
	BusinessHomePage     string           `json:"businessHomePage,omitempty" bson:"business_home_page,omitempty"`
	BusinessPhones       []string         `json:"businessPhones,omitempty" bson:"business_phones,omitempty"`
	Categories           []string         `json:"categories,omitempty" bson:"categories,omitempty"`
	ChangeKey            string           `json:"changeKey,omitempty" bson:"change_key,omitempty"`
	Children             []string         `json:"children,omitempty" bson:"children,omitempty"`
	CompanyName          string           `json:"companyName,omitempty" bson:"company_name,omitempty"`
	CreatedDateTime      string           `json:"createdDateTime,omitempty" bson:"created_date_time,omitempty"`
	Department           string           `json:"department,omitempty" bson:"department,omitempty"`
	DisplayName          string           `json:"displayName,omitempty" bson:"display_name,omitempty"`
	EmailAddresses       []EmailAddress   `json:"emailAddresses,omitempty" bson:"email_addresses,omitempty"`
	FileAs               string           `json:"fileAs,omitempty" bson:"file_as,omitempty"`
	Generation           string           `json:"generation,omitempty" bson:"generation,omitempty"`
	GivenName            string           `json:"givenName,omitempty" bson:"given_name,omitempty"`
	HomeAddress          *PhysicalAddress `json:"homeAddress,omitempty" bson:"home_address,omitempty"`
	HomePhones           []string         `json:"homePhones,omitempty" bson:"home_phones,omitempty"`
	ImAddresses          []string         `json:"imAddresses,omitempty" bson:"im_addresses,omitempty"`
	Initials             string           `json:"initials,omitempty" bson:"initials,omitempty"`
	JobTitle             string           `json:"jobTitle,omitempty" bson:"job_title,omitempty"`
	LastModifiedDateTime string           `json:"lastModifiedDateTime,omitempty" bson:"last_modified_date_time,omitempty"`
	Manager              string           `json:"manager,omitempty" bson:"manager,omitempty"`
	MiddleName           string           `json:"middleName,omitempty" bson:"middle_name,omitempty"`
	MobilePhone          string           `json:"mobilePhone,omitempty" bson:"mobile_phone,omitempty"`
	NickName             string           `json:"nickName,omitempty" bson:"nick_name,omitempty"`
	OfficeLocation       string           `json:"officeLocation,omitempty" bson:"office_location,omitempty"`
	OtherAddress         *PhysicalAddress `json:"otherAddress,omitempty" bson:"other_address,omitempty"`
	ParentFolderID       string           `json:"parentFolderId,omitempty" bson:"parent_folder_id,omitempty"`
	PersonalNotes        string           `json:"personalNotes,omitempty" bson:"personal_notes,omitempty"`
	Profession           string           `json:"profession,omitempty" bson:"profession,omitempty"`
	SpouseName           string           `json:"spouseName,omitempty" bson:"spouse_name,omitempty"`
	Surname              string           `json:"surname,omitempty" bson:"surname,omitempty"`
	Title                string           `json:"title,omitempty" bson:"title,omitempty"`
	YomiCompanyName      string           `json:"yomiCompanyName,omitempty" bson:"yomi_company_name,omitempty"`
	YomiGivenName        string           `json:"yomiGivenName,omitempty" bson:"yomi_given_name,omitempty"`
	YomiSurname          string           `json:"yomiSurname,omitempty" bson:"yomi_surname,omitempty"`
	Extension            []*Extension     `json:"extensions,omitempty" bson:"extension,omitempty"`
	modelImpl
}

func (c *Contact) FromJson(data io.Reader) (interface{}, error) {
	model, err := c.modelImpl.FromJson(data, c)
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (c *Contact) PreSave() {
	UID := uuid.NewUUID()

	if len(c.ID) == 0 {
		log.Println(UID)
		c.ID = base64.URLEncoding.EncodeToString(UID)
	}

	if len(c.Extension) != 0 {
		for _, extension := range c.Extension {
			extension.Create()
		}
	}
}
