package model

import "log"

type Extension map[string]interface{}

func (e *Extension) Update() {
	panic("implement me")
}

func (e *Extension) Get() {
	panic("implement me")
}

func (e *Extension) Delete() {
	panic("implement me")
}

func (e Extension) Create() {
	log.Println("e:-", e)
	e["id"] = e["extensionName"]
}
