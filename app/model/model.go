package model

import (
	"encoding/json"
	"io"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Model interface {
	ToJson(model interface{}) ([]byte, error)
	ToBson(model interface{}) ([]byte, error)
	FromJson(data io.Reader, model interface{}) (interface{}, error)
	FromBson(props primitive.M, model interface{}) (interface{}, error)
}

type modelImpl struct{}

func (m *modelImpl) ToJson(model interface{}) ([]byte, error) {
	v, err := json.Marshal(model)
	if err != nil {
		return nil, err
	}
	return v, nil
}

func (m *modelImpl) ToBson(model interface{}) ([]byte, error) {
	b, err := bson.Marshal(model)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func (m *modelImpl) FromJson(data io.Reader, model interface{}) (interface{}, error) {
	decoder := json.NewDecoder(data)
	if err := decoder.Decode(model); err != nil {
		return nil, err
	}
	return model, nil
}

func (m *modelImpl) FromBson(props primitive.M, model interface{}) (interface{}, error) {
	data, err := bson.MarshalExtJSON(props, false, false)
	if err != nil {
		return nil, err
	}

	err = bson.UnmarshalExtJSON(data, false, model)
	return model, err
}
