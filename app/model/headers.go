package model

// Headers
const (
	HeaderRequestID        = "X-Request-ID"
	HeaderVersionID        = "X-Version-ID"
	HeaderClusterID        = "X-Cluster-ID"
	HeaderEtagServer       = "ETag"
	HeaderEtagClient       = "If-None-Match"
	HeaderForwarded        = "X-Forwarded-For"
	HeaderRealIP           = "X-Real-IP"
	HeaderForwardedProto   = "X-Forwarded-Proto"
	HeaderToken            = "token"
	HeaderBearer           = "BEARER"
	HeaderAuth             = "Authorization"
	HeaderRequestedWith    = "X-Requested-With"
	HeaderRequestedWithXML = "XMLHttpRequest"
	Status                 = "status"
	StatusOK               = "OK"
	StatusFail             = "FAIL"
	StatusRemove           = "REMOVE"
)
