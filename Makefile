APP = app

.PHONY: all
all:run

.PHONY: build
build:
	$(MAKE) -C $(APP) all

.PHONY: run
run: build start-proxy
	 $(MAKE) -C $(APP) run

.PHONY: start-proxy
start-proxy:
	sudo $(MAKE) -C sidecar \
		-e APP_KEY=$(APP_KEY) \
		-e APP_ID=$(APP_ID) \
		-e APP_NAME=$(APP_NAME) \
		start

.PHONY: stop
stop:
	rm -r $(APP)/bin
	sudo $(MAKE) -C sidecar stop

.PHONY: dist
dist: ; $(info building dist tarball ...)
	$(MAKE) -C $(APP) dist
	@mv $(APP)/dist ./

.PHONY: test
test:
	$(MAKE) -C $(APP) test

.PHONY: lint
lint:
	$(MAKE) -C $(APP) lint
