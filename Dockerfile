# This file is a template, and might need editing before it works on your project.
FROM golang

WORKDIR /data/contact
COPY . .
CMD ["make"]