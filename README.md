# contact

contact microservice

## Dependancy
1. Caddy
2. Go 1.19 +
3. golangci-lint

# Sidecar configuration

## Proxy loader configuration

Add following configuration into `admin` section to comunicate with porxy manager.
It will inject the sidecar configuration

```
"config":{
  "load":{
    "module": <<Module type>>,
    "method": <<Http method>>,
    "url": <<porxy manage endpoint>>,
    "tls":{
      "client_certificate_file": <<Certificate>>,
      "client_certificate_key_file": <<Certificate key>>,
      "root_ca_pem_files": [<<Root CA>>]
    },
    "timeout": <<Time out>>
  }
}
```

## Example
```
{
	"admin": {
		"listen": CADDY_ADMIN_API,
		"config":{
			"load":{
				"module": "http",
				"method": "GET",
				"url": VAULT_URL,
				"header": {
					"App-Host-Name": ["{system.hostname}"],
					"APP-CONTAINER-OS": ["{system.os}"],
				 	"APP_KEY": ["{env.APP_KEY}"],
					"APP_ID": ["{env.APP_ID}"],
					"host-url":["{http.request.uri}"]
			 	},
				"tls":{
					"client_certificate_file": CADDY_CERTIFICATE,
					"client_certificate_key_file": CADDY_KEY,
					"root_ca_pem_files": [CADDY_ROOT_CA]
				},
				"timeout": "2s"
			}
		}
	},
	"logging":{
		"logs":{
			"default": {
				"level":"DEBUG",
				"include":[
					"http.log.access.default",
					"http.handlers.reverse_proxy"
				],
				"encoder":{
					"format": "filter",
					"wrap":{
						"format": "json",
						"message_key":"log-message",
						"name_key": "contact.lxd"
					},
					"fields":{
						"ts":{
							"filter": "delete"
						}
					}
				}
			}
		}
	}
}
```
